﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MeterReadings.WebApi.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace MeterReadings.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class ApiController : Controller
    {
        private readonly ApplicationContext _context;

        public ApiController(ApplicationContext context)
        {
            _context = context;
        }

        //Получение списка улиц
        [HttpGet]
        public ActionResult GetAllStreets()
        {
            var streets = _context.Streets
                .Where(x => !string.IsNullOrEmpty(x.StreetName))
                .OrderBy(x => x.StreetName)
                .ToList();

            return Json(streets);
        }

        //Получение списка домов
        [HttpGet]
        public ActionResult GetHouses(long streetId)
        {
            var houses = _context.Houses
                .Where(x => x.StreetId == streetId)
                .OrderBy(x => x.HouseNumber)
                .ToList();

            return Json(houses);
        }

        //Получение списка квартир
        [HttpGet]
        public ActionResult GetApartments(long houseId)
        {
            var apartments = _context.Apartments
                .Where(x => x.HouseId == houseId)
                .OrderBy(x => x.ApartmentNumber)
                .ToList();

            return Json(apartments);
        }

        //Получение последнего показания частного дома
        [HttpGet]
        public ActionResult GetLastReadingByHouseId(long houseId)
        {
            var lastReading = _context.Readings
                .Where(x => x.HouseId == houseId)
                .OrderByDescending(x => x.DateTime)
                .FirstOrDefault();

            return Json(lastReading);
        }

        //Получение последнего показания квартиры
        [HttpGet]
        public ActionResult GetLastReadingByApartmentId(long apartmentId)
        {
            var lastReading = _context.Readings
                .Where(x => x.ApartmentId == apartmentId)
                .OrderByDescending(x => x.DateTime)
                .FirstOrDefault();

            return Json(lastReading);
        }

        //Получение истории показаний
        [HttpGet]
        public ActionResult GetLast10Readings()
        {
            var last10Readings = _context.Readings
                .Include(x => x.House)
                .Include(x => x.Apartment)
                .Include("House.Street")
                .Include("Apartment.House")
                .Include("Apartment.House.Street")
                .OrderByDescending(x => x.DateTime)
                .Take(10)
                .ToList();

            var formetedStrings = last10Readings.Select(x =>
            {
                // если есть HouseId, тогда это частный дом
                if (x.HouseId.HasValue)
                {
                    return $"Улица: {x.House.Street.StreetName}, " +
                    $"Частный дом №{x.House.HouseNumber}, " +
                    $"Показания: {x.ReadingForElectricity}, " +
                    $"Дата: {x.DateTime.ToString("dd.MM.yyyy")}";
                }
                else
                {
                    return $"Улица:{x.Apartment.House.Street.StreetName}, " +
                    $"Дом №{x.Apartment.House.HouseNumber}, " +
                    $"Квартира №{x.Apartment.ApartmentNumber}, " +
                    $"Показания: {x.ReadingForElectricity}, " +
                    $"Дата: {x.DateTime.ToString("dd.MM.yyyy")}";
                }

            }).ToList();

            return Json(formetedStrings);
        }

        //Получение истории показаний частного дома
        [HttpGet]
        public ActionResult GetReadingsByHouseId(long houseId)
        {
            var lastReadings = _context.Readings
                .Include(x => x.House)
                .Include(x => x.Apartment)
                .Include("House.Street")
                .Include("Apartment.House")
                .Include("Apartment.House.Street")
                .Where(x => x.HouseId == houseId)
                .OrderByDescending(x => x.DateTime)
                .ToList();

            var formetedStrings = lastReadings.Select(x =>
            {
                return $"Дата: {x.DateTime.ToString("dd.MM.yyyy")}, " +
                $"Улица: {x.House.Street.StreetName}, " +
                    $"Частный дом №{x.House.HouseNumber}, " +
                    $"Показания: {x.ReadingForElectricity} ";


            }).ToList();

            return Json(formetedStrings);
        }

        //Получение истории показаний квартиры
        [HttpGet]
        public ActionResult GetReadingsByApartmentId(long apartmentId)
        {
            var lastReadings = _context.Readings
                .Include(x => x.House)
                .Include(x => x.Apartment)
                .Include("House.Street")
                .Include("Apartment.House")
                .Include("Apartment.House.Street")
                .Where(x => x.ApartmentId == apartmentId)
                .OrderByDescending(x => x.DateTime)
                .ToList();

            var formetedStrings = lastReadings.Select(x =>
            {
                return $"Дата: {x.DateTime.ToString("dd.MM.yyyy")}, " +
                $"Улица: {x.Apartment.House.Street.StreetName}, " +
                    $"Дом №{x.Apartment.House.HouseNumber}, " +
                    $"Квартира №{x.Apartment.ApartmentNumber}, " +
                    $"Показания: {x.ReadingForElectricity} ";
            }).ToList();

            return Json(formetedStrings);
        }

        //Сохранение показания
        [HttpPost]
        public ActionResult SaveReading(Reading reading)
        {
            var dbReading = _context.Readings
                .FirstOrDefault(x => x.ReadingId == reading.ReadingId);

            if (dbReading == null)
            {
                dbReading = new Reading
                {
                    ReadingForElectricity = reading.ReadingForElectricity,
                    DateTime = DateTime.UtcNow,
                    HouseId = reading.HouseId,
                    ApartmentId = reading.ApartmentId
                };

                _context.Readings.Add(dbReading);
            }
            else
            {
                dbReading.ReadingForElectricity = reading.ReadingForElectricity;
            }

            _context.SaveChanges();
            return Json(dbReading);
        }
    }

}