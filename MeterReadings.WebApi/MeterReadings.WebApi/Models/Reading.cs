﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MeterReadings.WebApi.Models
{
    public class Reading
    {
        [Key]
        public long ReadingId { get; set; }

        public decimal ReadingForElectricity { get; set; }

        public DateTime DateTime { get; set; }

        [ForeignKey("House")]
        public long? HouseId { get; set; }

        public House House { get; set; }

        [ForeignKey("Apartment")]
        public long? ApartmentId { get; set; }

        public Apartment Apartment { get; set; }

    }
}
