﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MeterReadings.WebApi.Models
{
    public class House
    {
        [Key]
        public long HouseId { get; set; }

        public string HouseNumber { get; set; }

        public bool IsPrivateHouse { get; set; }

        [ForeignKey("Street")]
        public long StreetId { get; set; }

        public Street Street { get; set; }

        public List<Apartment> Apartments { get; set; }

        public List<Reading> Readings { get; set; }
    }
}
