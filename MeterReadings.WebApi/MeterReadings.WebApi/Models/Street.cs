﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MeterReadings.WebApi.Models
{
    public class Street
    {
        [Key]
        public long StreetId { get; set; }

        public string StreetName { get; set; }

        public List<House> Houses { get; set; }

    }
}
