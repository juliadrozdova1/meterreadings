﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MeterReadings.WebApi.Models
{
    public class Apartment
    {
        [Key] 
        public long ApartmentId { get; set; }

        public long  ApartmentNumber{ get; set; }

        [ForeignKey("House")]
        public long HouseId { get; set; }
        public House House { get; set; }

        public List<Reading> Readings { get; set; }
    }
}
